<?php

namespace App\Shop\Application\Handler;

use App\Shop\Application\Command\CreateORMProduct;
use App\Shop\Domain\Model\Product;
use App\Shop\Infrastructure\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Messenger\MessageBusInterface;

class CreateORMProductHandler implements MessageHandlerInterface
{
    public function __construct(
        private ProductRepository $repository,
    ) {
    }

    public function __invoke(CreateORMProduct $command)
    {
        $product = new Product($command->id);

        $product->name = $command->name;

        $this->repository->save($product);
    }
}
