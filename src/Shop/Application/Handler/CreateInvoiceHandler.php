<?php

namespace App\Shop\Application\Handler;

use App\Shop\Application\Command\CreateInvoice;
use App\Shop\Application\Service\InvoiceMaker;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateInvoiceHandler implements MessageHandlerInterface
{
    public function __construct(
        private InvoiceMaker $invoiceMaker,
    ) {
    }

    public function __invoke(CreateInvoice $command)
    {

    }
}
