<?php

namespace App\Shop\Application\Handler;

use App\Shop\Application\Query\FindProductQuery;
use App\Shop\Infrastructure\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class FindProductHandler implements MessageHandlerInterface
{
    public function __construct(
        private ProductRepository $repository,
    ) {
    }

    public function __invoke(FindProductQuery $query): \App\Shop\Domain\Model\Product
    {
       $product = $this->repository->find($query->id);

       $shopProduct = new \App\Shop\Domain\Model\Product($query->id);
       $shopProduct->id = $query->id;
       $shopProduct->name = $product->getName();

       return $shopProduct;
    }
}
