<?php

namespace App\Shop\Application\Command;

use App\Shop\Domain\Model\ProductId;
use App\Shop\Infrastructure\ApiPlatform\Product;
use JetBrains\PhpStorm\Pure;

class CreateORMProduct
{
    public string $id;
    public string $name;

    #[Pure] public function __construct(
        Product $product,
    )
    {
        $productId = new ProductId();
        $this->id = $productId->value;
        $this->name = $product->getName();
    }
}
