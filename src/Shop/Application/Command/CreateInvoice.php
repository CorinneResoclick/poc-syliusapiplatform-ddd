<?php

namespace App\Shop\Application\Command;

use App\Shop\Infrastructure\ApiPlatform\Order;
use App\Shop\Infrastructure\ApiPlatform\Product;

class CreateInvoice
{
    public function __construct(
        public Product $product,
        public Order $order,
    )
    {
    }
}
