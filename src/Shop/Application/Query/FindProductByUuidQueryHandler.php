<?php

namespace App\Shop\Application\Query;

use App\Shop\Domain\Repository\ProductRepositoryInterface;
use App\Shop\Infrastructure\DoctrineMapping\Product;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class FindProductByUuidQueryHandler implements MessageHandlerInterface
{
    public function __construct(
        private ProductRepositoryInterface $productRepository,
    ) {
    }

    public final function __invoke(FindProductByUuidQuery $query): ?Product
    {
        return $this->productRepository->findByUuid($query->id);
    }

}
