<?php

namespace App\Shop\Application\Query;

use JetBrains\PhpStorm\Pure;

final class FindProductByUuidQuery
{
    #[Pure] public function __construct(
        public string $id,
    )
    {
    }
}
