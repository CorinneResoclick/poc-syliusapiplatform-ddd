<?php

namespace App\Shop\Application\Query;

use JetBrains\PhpStorm\Pure;

class FindProductQuery
{
    #[Pure] public function __construct(
        public string $id,
    )
    {
    }
}
