<?php

namespace App\Shop\Application\EventSubscriber;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Shop\Application\Command\CreateInvoice;
use App\Shop\Infrastructure\ApiPlatform\Order;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Messenger\MessageBusInterface;

class OnCreateOrderEventSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private MessageBusInterface $messageBus,
    )
    {
    }

    #[ArrayShape([KernelEvents::VIEW => "array"])] public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['persist', EventPriorities::PRE_RESPOND],
        ];

    }

    public function persist(ViewEvent $event): void
    {
        $order = $event->getRequest()->attributes->get('data');

        if (!$order instanceof Order) {
            return;
        }

        $createInvoice = new CreateInvoice($order);

        $this->messageBus->dispatch($createInvoice);
    }
}
