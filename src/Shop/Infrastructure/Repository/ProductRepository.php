<?php

namespace App\Shop\Infrastructure\Repository;

use App\Shop\Domain\Repository\ProductRepositoryInterface;
use App\Shop\Infrastructure\DoctrineMapping\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Shop\Domain\Model\Product as ShopProduct;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $productBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $productBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository implements ProductRepositoryInterface
{
    private ManagerRegistry $registry;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
        $this->registry = $registry;
    }

    public function save(ShopProduct|Product $product): void
    {
        if ($product instanceof ShopProduct) {
            $ormProduct = new Product();
            $ormProduct->setId($product->id);
            $ormProduct->setName($product->name);

            $product = $ormProduct;
        }

        $this->registry->getManager()->persist($product);
        $this->registry->getManager()->flush();
        $this->registry->getManager()->clear();
    }

    public function delete($id)
    {
        $product = $this->registry->getRepository(Product::class)->find($id);

        $this->registry->getManager()->remove($product);
        $this->registry->getManager()->flush();
        $this->registry->getManager()->clear();
    }

    public function findByUuid($uuid)
    {
        return $this->registry->getRepository(Product::class)->find($uuid);
    }
}
