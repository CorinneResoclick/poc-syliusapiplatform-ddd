<?php

namespace App\Shop\Infrastructure\ApiPlatform;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[ApiResource(
    types: ['https://schema.org/Order'],
    operations: [
        new Get(),
        new GetCollection(),
        new Post(
            uriTemplate: '/orders',
            status: 202,
            output: false,
            //messenger: true,
        ),
        new Put(),
        new Patch(),
        new Delete(),
    ]
)]
class Order
{
    #[ApiProperty(identifier: true)]
    protected int $id;

    #[Assert\NotNull]
    #[ApiProperty]
    protected \DateTimeImmutable $orderDate;

    #[Assert\NotNull]
    #[ApiProperty]
    protected float $amount;

    #[Assert\NotNull]
    #[ApiProperty]
    protected int $orderNumber;

    #[Assert\NotNull]
    #[ApiProperty]
    protected string $orderStatus;

    #[ApiProperty]
    protected ArrayCollection $products;

    public function __construct(
    ){
        $this->products = new ArrayCollection();
        $this->orderDate = new \DateTimeImmutable();
    }

    #[Pure] public function getId(): string
    {
        return $this->id;
    }

    public function getOrderDate(): \DateTimeImmutable
    {
        return $this->orderDate;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getProducts(): iterable
    {
        return $this->products;
    }

    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    public function setOrderNumber(int $orderNumber): void
    {
        $this->orderNumber = $orderNumber;
    }

    public function setOrderStatus(string $orderStatus): void
    {
        $this->orderStatus = $orderStatus;
    }

    public function addProduct(Product $item): void
    {
        if ($this->products->contains($item)) {
            return;
        }
        $this->products->add($item);
    }

    public function removeProduct(Product $item): void
    {
        if (! $this->products->contains($item)) {
            return;
        }
        $this->products->removeElement($item);
    }
}
