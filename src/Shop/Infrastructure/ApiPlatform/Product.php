<?php

namespace App\Shop\Infrastructure\ApiPlatform;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Shop\Application\Query\FindProductByUuidQuery;
use App\Shop\Infrastructure\Service\State\ProductProcessor;
use App\Shop\Infrastructure\Service\State\ProductProvider;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[ApiResource(
    types: ['https://schema.org/Product'],
    operations: [
        new Get(
            provider: ProductProvider::class,
            options: ['query' => FindProductByUuidQuery::class]
        ),
        new GetCollection(
            provider: ProductProvider::class
        ),
        new Post(
            messenger: true,
            output: false,
            status: 202,
            processor: ProductProcessor::class
        ),
        new Put(),
        new Patch(),
        new Delete(),
    ]
)]
class Product
{
    #[ApiProperty(identifier: true)]
    protected ?string $id = null;

    #[ApiProperty]
    #[Assert\NotNull]
    protected string $name;

    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
