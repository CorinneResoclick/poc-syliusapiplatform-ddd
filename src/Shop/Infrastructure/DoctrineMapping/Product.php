<?php

namespace App\Shop\Infrastructure\DoctrineMapping;

use App\Shop\Infrastructure\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue(strategy:"NONE")]
    protected string $id;

    #[ORM\Column]
    #[Assert\NotNull]
    protected string $name;

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
