<?php

namespace App\Shop\Infrastructure\DoctrineMapping;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class OrderProduct
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    protected int $id;

    #[ORM\ManyToOne(targetEntity: Order::class, inversedBy: 'products')]
    protected Order $order;

    #[ORM\ManyToOne(targetEntity: Product::class)]
    protected Product $orderedItem;

    #[ORM\Column]
    #[Assert\NotNull]
    protected int $orderQuantity;

    #[ORM\Column]
    #[Assert\NotNull]
    protected string $orderItemStatus;

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function getOrderedItem(): Product
    {
        return $this->orderedItem;
    }

    public function getOrderQuantity(): int
    {
        return $this->orderQuantity;
    }

    /**
     * @return string
     */
    public function getOrderItemStatus(): string
    {
        return $this->orderItemStatus;
    }
}
