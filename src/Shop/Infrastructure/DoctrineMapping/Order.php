<?php

namespace App\Shop\Infrastructure\DoctrineMapping;

use App\Shop\Infrastructure\Repository\OrderRepository;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
class Order
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    protected int $id;

    #[ORM\Column]
    #[Assert\NotNull]
    protected \DateTimeImmutable $orderDate;

    #[ORM\Column]
    #[Assert\NotNull]
    protected float $amount;

    #[ORM\Column]
    #[Assert\NotNull]
    protected int $orderNumber;

    #[ORM\Column]
    #[Assert\NotNull]
    protected string $orderStatus;

    #[ORM\OneToMany(mappedBy: 'order', targetEntity: Product::class, cascade: ['persist'])]
    #[ORM\JoinTable(name: 'orderProduct')]
    #[ORM\JoinColumn(name: 'order', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'product', referencedColumnName: 'id', unique: true)]
    protected ArrayCollection $products;

    public function __construct(
    ){
        $this->products = new ArrayCollection();
    }

    #[Pure] public function getId(): string
    {
        return $this->id;
    }

    public function getOrderDate(): \DateTimeImmutable
    {
        return $this->orderDate;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getProducts(): iterable
    {
        return $this->products;
    }

    public function addProduct(Product $item): void
    {
        if ($this->products->contains($item)) {
            return;
        }
        $this->products->add($item);
    }

    public function removeProduct(Product $item): void
    {
        if (! $this->products->contains($item)) {
            return;
        }
        $this->products->removeElement($item);
    }

    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    public function setOrderNumber(int $orderNumber): void
    {
        $this->orderNumber = $orderNumber;
    }

    public function setOrderStatus(string $orderStatus): void
    {
        $this->orderStatus = $orderStatus;
    }


}
