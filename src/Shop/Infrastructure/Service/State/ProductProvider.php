<?php

namespace App\Shop\Infrastructure\Service\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Shop\Application\Query\FindProductByUuidQuery;
use App\Shop\Application\Query\ListProductQuery;
use App\Shop\Infrastructure\ApiPlatform\Product;
use App\Shop\Infrastructure\Repository\ProductRepository;
use Symfony\Component\Messenger\MessageBusInterface;

final class ProductProvider implements ProviderInterface
{
    public function __construct(
        private MessageBusInterface $queryBus,
        private ProductRepository $repository,
    ) {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): Product|array
    {
        if ($operation instanceof CollectionOperationInterface) {
            $ormProducts = $this->repository->findAll();

            $apiProducts = [];
            foreach ($ormProducts as $product) {
                $apiProduct = $this->getApiProduct($product);
                $apiProducts[] = $apiProduct;
            }

            return $apiProducts;
        }

        $query = $operation->getOptions()['query'];

        $this->queryBus->dispatch(new $query($uriVariables['id']));

        return $this->getApiProduct($this->repository->find($uriVariables['id']));
    }

    private function getApiProduct(\App\Shop\Infrastructure\DoctrineMapping\Product $product): Product
    {
        $apiProduct = new Product();
        $apiProduct->setId($product->getId());
        $apiProduct->setName($product->getName());

        return $apiProduct;
    }
}
