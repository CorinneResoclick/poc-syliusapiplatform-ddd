<?php

namespace App\Shop\Infrastructure\Service\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Shop\Application\Command\CreateORMProduct;
use Symfony\Component\Messenger\MessageBusInterface;

class ProductProcessor implements ProcessorInterface
{
    public function __construct(
        private MessageBusInterface $messageBus,
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function process($data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        $createProduct = new CreateORMProduct($data);

        $this->messageBus->dispatch($createProduct);
    }
}
