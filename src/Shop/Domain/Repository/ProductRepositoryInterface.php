<?php

namespace App\Shop\Domain\Repository;

use App\Shop\Infrastructure\DoctrineMapping\Product;

interface ProductRepositoryInterface
{
    public function find($id, $lockMode = null, $lockVersion = null);
    public function findAll();
    public function findBy(array $criteria, array $productBy = null, $limit = null, $offset = null);
    public function findOneBy(array $criteria, array $productBy = null);
    public function save(Product $product): void;
    public function findByUuid($uuid);
}
