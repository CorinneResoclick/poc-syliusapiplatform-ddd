<?php

namespace App\Shop\Domain\Repository;

use App\Shop\Domain\Model\Order;

interface OrderRepositoryInterface
{
    public function find($id, $lockMode = null, $lockVersion = null);

    public function save(Order $order): void;

    public function delete($id): void;
}
