<?php

namespace App\Shop\Domain\Model;

use JetBrains\PhpStorm\Pure;

class Product
{
    #[Pure] public function __construct(
        public string $id,
    ) {
    }

    public string $name;
}
