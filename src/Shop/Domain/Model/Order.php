<?php

namespace App\Shop\Domain\Model;

use JetBrains\PhpStorm\Pure;

class Order
{
    protected int $id;

    protected \DateTimeImmutable $orderDate;

    protected float $amount;

    protected int $orderNumber;

    protected string $orderStatus;

    protected array $products;

    #[Pure] public function __construct(
    ){
        $this->products = [];
    }

    #[Pure] public function getId(): string
    {
        return $this->id;
    }

    public function getOrderDate(): \DateTimeImmutable
    {
        return $this->orderDate;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getProducts(): iterable
    {
        return $this->products;
    }

    public function addProduct(Product $item): void
    {
        if (\in_array($item, $this->products)) {
            return;
        }

        $this->products[] = $item;
    }

    public function removeProduct(Product $item): void
    {
        if (! \in_array($item, $this->products)) {
            return;
        }

        unset($item, $this->products);
    }
}
