<?php

namespace App\Shop\Domain\Model;

class ProductId
{
    public string $value;

    public function __construct(
    ) {
        $this->value = uniqid();
    }
}
