<?php

namespace App\Shop\Domain\Model;

class OrderProduct
{
    protected Order $order;

    protected Product $orderedItem;

    protected int $orderQuantity;

    protected string $orderItemStatus;

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function getOrderedItem(): Product
    {
        return $this->orderedItem;
    }

    public function getOrderQuantity(): int
    {
        return $this->orderQuantity;
    }

    public function getOrderItemStatus(): string
    {
        return $this->orderItemStatus;
    }
}
